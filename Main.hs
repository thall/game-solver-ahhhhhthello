module Main where
import Data.List.Split
import Data.Char
import Data.Maybe
import System.Environment
import System.Console.GetOpt
import Othello
import PrintIO
import OTypes

data Flag = Help | Winner | Verbose | Interactive | Depth String | Move String deriving (Eq, Show)

options :: [OptDescr Flag]
options = [ Option ['h'] ["help"] (NoArg Help) "Print usage information and exit."
          , Option ['w'] ["winner"] (NoArg Winner) "Print out the best move, using an exhaustive search (no cut-off depth)."
          , Option ['v'] ["verbose"] (NoArg Verbose) "Output both the move and a description of how good it is."
          , Option ['d'] ["depth"] (ReqArg Depth "<num>") "Use a cutoff depth of <num>."
          , Option ['m'] ["move"] (ReqArg Move "<move>") "Read in gamestate <move> and outputs a raw game state. The format of <move> is \"(x,y)\"."
          , Option ['i'] ["interactive"] (NoArg Interactive) "Start a game against an AI player. If no game file is specified a default is used."
          ]

---- general use helper functions:
--

-- Prompt the user for an input string; the string arg is the prompt text
prompt :: String -> IO String
prompt line =  do
  putStrLn line
  name <- getLine
  return name

---- various constants:
--
helpStr :: String
helpStr = usageInfo "Usage: othello [OPTIONS...] [gamefile]" options


---- fetching arguments and flags:
--

-- get the game from the file
getGame :: Maybe Game -> Game
getGame mgame = case mgame of
                  Nothing -> error "Invalid Game File. \n"
                  Just g -> g

-- get the depth AKA cutoff integer
getCutoff :: [Flag] -> Int
getCutoff [] = 4 -- default cutoff depth
getCutoff ((Depth s):_) = if (all isDigit s) then read s else error "Depth must be an integer"
getCutoff (_:flags) = getCutoff flags

-- get the move string if one exists
getMove :: [Flag] -> Cord
getMove [] = (-1,-1) -- There was no move; this is a Cord that should always be invalid
getMove ((Move m):_) = parseMove m
getMove (_:flags) = getMove flags

-- helper for getMove that is also used in interactive without getMove
parseMove :: String -> Cord
parseMove [] = (-10,-10) -- invalid input
parseMove m =
  if (isFormatted)
  then let cordNums = map read cordStrings
           fstN = cordNums !! 0
           sndN = cordNums !! 1
       in (fstN-1,sndN-1) -- expected to be zero-indexed
  else (-10,-10) -- invalid input
  where
    cordStrings = filter (not . null) $ splitOneOf "()," m
    isFormatted = (length cordStrings == 2) && (all (\x -> all isDigit x) cordStrings) 

-- returns true if we are using a cutoff for recursive predictions
isCutoff :: [Flag] -> Bool
isCutoff [] = True -- default is there is a cutoff
isCutoff flags = not $ Winner `elem` flags

-- returns true if we are using a cutoff for recursive predictions
isVerbose :: [Flag] -> Bool
isVerbose [] = False -- default is there is a cutoff
isVerbose flags = Verbose `elem` flags

---- main helpers (IO mostly)
--

-- print the string which indicates the move (x,y) player p should make
bestMoveString :: Space -> String
bestMoveString ((x,y),p) = mString
  where
    pStr = if(p==White) then "White" else "Black"
    xStr = show x
    yStr = show y -- x and y may need to be reversed here -- needs testing
    mvStr = "("++xStr++","++yStr++")"
    mString = "The best move for "++pStr++" is "++mvStr

---- main
--
  
main :: IO ()
main = do
  args <- getArgs
  let
    (flags, others, errors) = getOpt Permute options args
    askedHelp = Help `elem` flags
  if(askedHelp)
    then do putStrLn helpStr
    else -- do regular behaviours here
    do
      if (Interactive `elem` flags)
        then do playInteractive flags others -- dispatch to interactive
        else
        do
          if (null others)
            then error "Missing game file argument."
            else
            do
              let fname = head others
              mgame <- readGameFile fname
              let
                inGame = getGame mgame
                move = getMove flags
              if (move==(-10,-10))
                then error "Invalid Move formatting \n ex: (1,1)"
                else do runOthello inGame move flags

-- dispatcher to playMove, predict, and predictEndless
runOthello :: Game -> Cord -> [Flag] -> IO ()
runOthello inGame move flags =
  do
    let makingMove = if (move==(-1,-1)) 
												then False else True -- (-1,-1) indicates non-move mode
    if (makingMove)
      then do playMove inGame move
      else
      do -- print out the bestMove, optionally with no cutoff depth
        let limitless = not $ isCutoff flags
        if (limitless)
          then do predictEndless inGame move flags
          else do predict inGame move flags

-- take in a game and a coordinate and make a new game with that coordinate to output
playMove :: Game -> Cord -> IO ()
playMove inGame move =
  do
    let mNextGame = doMove inGame move
    case mNextGame of
      Nothing -> error "The specified move was not legal for the current player."
      Just outGame -> let output = mkOutGame outGame
                      in do putStr output

-- get bestMove with no cutoff
predictEndless :: Game -> Cord -> [Flag] -> IO ()
predictEndless inGame move flags =
  do
    let
      (retMove,winState) = bestMove inGame
      moveString = bestMoveString retMove
      winStr =
        case winState of
          Won White -> "a win for the white piece."
          Won Black -> "a win for the black piece."
          Tie -> "a tie."
      outComeString = "\n The outcome will be "++winStr
    if (isVerbose flags)
      then do putStrLn $ moveString++outComeString
      else do putStrLn moveString

-- get bestMove with a cutoff
predict :: Game -> Cord -> [Flag] -> IO ()
predict inGame move flags =
  do
    let
      cutoff = getCutoff flags
      (mretMove,Score mvRating) = goodMove inGame cutoff
      rateStr =
        case () of _
                     | mvRating > 0 -> " The white piece will probably win."
                     | mvRating < 0 -> " The black piece will probably win."
                     | otherwise -> " It will probably end in a Tie."
    case mretMove of
      Nothing -> putStr "The game was already over."
      Just m ->
        do
          let moveString = bestMoveString m
          if (isVerbose flags)
            then do putStrLn $ moveString++rateStr
            else do putStrLn moveString
---- Interactive Mode
-- (great pyramids ahead)

-- start a recursive play mode using -d for AI
playInteractive :: [Flag] -> [String] -> IO ()
playInteractive flags others =
  do -- allow them to read in a game manually as well, but default to startboard
    mgame <- if(null others)
             then readGameFile "testboards/startboard"
             else readGameFile $ head others
    let
      game = getGame mgame
      cutoff = getCutoff flags
    playerLoop game cutoff

-- human's turn in the interactive loop
playerLoop :: Game -> Int -> IO ()
playerLoop game cutoff = -- check end condition?
  if (gameOver game)
  then do announceWinner game
  else
    if null $ legalMoves game
    then do aiLoop (otherGame game) cutoff
    else
      do
        putGame game -- print the game to the string in a fancy way
        rawMove <- prompt "Input a move in the format (x,y)"
        let move = parseMove rawMove
        if (move==(-10,-10))
          then
          do
            putStrLn "Invalid move formatting."
            playerLoop game cutoff -- try again
          else
          do -- move inputted was valid; continue
            let mgame = doMove game move
            case mgame of
              Nothing ->
                do
                  putStrLn "Sorry, that is an invalid play. Try again."
                  playerLoop game cutoff
              Just nextGame -> -- play was valid; continue
                do
                  putStrLn "AI Turn:"
                  putGame nextGame
                  aiLoop nextGame cutoff
                          
-- AI's turn in the interactive loop
aiLoop :: Game -> Int -> IO ()
aiLoop game cutoff =
  do -- check of game is over
    let possibleMoves = legalMoves game
    if (gameOver game) then do announceWinner game
      else if(null possibleMoves)
      then do playerLoop (otherGame game) cutoff
      else
      do
      let (mAIMove,_) = goodMove game cutoff
      case mAIMove of
        Nothing -> announceWinner game -- this will never happen, but whatever
        Just (aiMove,_) -> -- fromJust because move must be valid since AI used goodMove
          do
            let nextGame = fromJust $ doMove game aiMove
            playerLoop nextGame cutoff

-- announce the winner of an interactive game
announceWinner :: Game -> IO ()
announceWinner game =
  do
    let mWinner = whoHasWon game
    case mWinner of
      Nothing -> error "Something went wrong, announced winner of unfinished game."
      Just Tie -> putStrLn "The game is over! It was a tie!"
      Just (Won p) ->
        do
          let winStr = case p of
                         White -> "White"
                         Black -> "Black"
          putStrLn $ "The game is over! "++winStr++" has won!"
