module PrintIO
  ( putDefaultBoard
  , putBoard
  , putGame
  , readGameFile
  , writeGameFile
  , gameString
  , mkOutGame
  ) where
import Data.Tuple (swap)
import Data.List
import Data.Char
import OTypes

{-
File input functions:

readGameFile is a prompt for readGame.

readGame takes in a string of the raw
game file and then returns a new game state
with the correct board, linelength,
and current player turn.

Header format:
[Linelength integer] [Piece of current player]

Board format: A list of 0,1,2 divided into rows by newlines
where:
0 - Empty Space
1 - White Space
2 - Black Space

This is done by seperating the header information from the board information,
processing the header information (this doesn't require much explanation),
and then processing the board.

To process the board, all whitespace is removed from each individual line
and then any lines which were only whitespace are removed to avoid issues
with people who use editors which do not show whitespace.

-}
    

readGameFile :: String -> IO (Maybe Game)
readGameFile fileName =
  do
    rawGame <- readFile fileName
    return $ readGame rawGame

safeReadInt :: String -> Maybe Int
safeReadInt input =
  if (not $ all isDigit input)
  then Nothing
  else Just (read input)

readGame :: String -> Maybe Game
readGame rawGame =
  if (any (/=rawLength) lineLengths)
  then Nothing
  else
    case (builtBoard,cPlayer,lineLn) of
      (Just brd, Just pl, Just linum) -> Just $ Game brd pl linum $ [ (x, y) | x <- [0 .. (linum - 1)], y <- [0 .. (linum - 1)] ]
      otherwise -> Nothing
  where (fileHeader:rawBoard) = lines $ rawGame
        processedBoard = filter (not . null) $ map (filter (not . isSpace)) rawBoard
        (lineLnStr,cPlayerStr) = break (==' ') fileHeader
        rawLength = length rawBoard
        lineLn = safeReadInt lineLnStr
        cPlayer = getPlayer $ filter (not . isSpace) cPlayerStr
        builtBoard = buildBoard processedBoard
        lineLengths = [length line | line <- rawBoard]

{-
This is the function which builds the board taken in from the input.
It uses a helper function which recursively builds [String] with
a string for each row on the board.

Check if the board only contains the chars '1','2',and '0' or else
give back a nothing.
-}
buildBoard :: [String] -> Maybe Board
buildBoard rawBoard =
  if(boardCheck) then Nothing
  else 
    let
      ints = [0..] :: [Int]
      columnIndexedBoard = zip ints rawBoard
    in Just $ concatMap (\(y,row) -> buildRow y row) columnIndexedBoard
  where
    chars = concat rawBoard
    boardCheck = not $ all (`elem` validChars) chars
    validChars = "012"

-- takes the row number (y) and the string for the row to process
buildRow :: Int -> String -> [Space]
buildRow y row =
  [ case char of
      '1' -> ((x,y),White)
      '2' -> ((x,y),Black)
      otherwise -> error "invalid piece character found in board (not 1 or 2)"
    | (x,char) <- indexedCharLst, char /= '0']
  where ints = [0..] :: [Int]
        indexedCharLst = zip ints row
        
-- do some error handling
getPlayer :: String -> Maybe Player
getPlayer input
  | isInfixOf s "white" = Just White
  | isInfixOf s "black" = Just Black
  | otherwise = Nothing
  where s = map toLower input

{-
File output functions:

writeGameFile is a prompt for writeGame and
writes the board to a file specified by the
user.

mkOutGame takes in a Game and returns
a string in the same format as readGame's expected
input string.
-}


writeGameFile :: Game -> String -> IO ()
writeGameFile outGame fileName =
  do
    let fileString = mkOutGame outGame
    writeFile fileName fileString
    return ()

mkOutGame :: Game -> String
mkOutGame ogame = lnLength++" "++cPlayer++"\n"++brd++"\n"
  where
    cPlayer = show $ currentPlayer ogame
    lnLn = lineLength ogame
    lnLength = show $ lnLn
    brd = mkBoardString lnLn $ board ogame

-- use allCords to print every piece
mkBoardString :: Int -> Board -> String
mkBoardString lnLength brd = intercalate "\n" allSpaces
  where
    cordRows = [ [(y,x) | y <- [0..(lnLength-1)] ] | x <- [ 0 .. (lnLength-1) ] ]
    allSpaces = map (map (printSpace brd)) cordRows
-- y and x are flipped because the axises(sp?) are flipped on the printout
-- because 0 is at the top of the board

-- give the character of the piece at a coordinate
printSpace :: Board -> Cord -> Char
printSpace brd cord =
  case lookup cord brd of
    Nothing -> '0'
    Just White -> '1'
    Just Black -> '2'
    
{-
Printing functions:

putGame takes a Game and outputs a pretty
print of the Game state.

gameString makes a pretty printing string for the game.

putBoard takes a board and an integer for lineLength and
does a pretty print of the board.

putDefaultBoard is putBoard except the lineLength is assumed
to be 8.
-}

-- print a game to the screen
putGame :: Game -> IO ()
putGame game = putStr $ gameString game

-- make a string for a game
gameString :: Game -> String
gameString game = playerPrompt++gameBoard
  where
    playerPrompt = "It is "++(show (currentPlayer game)++"'s turn: \n")
    gameBoard = makeFramedPrint (board game) (lineLength game)

putBoard :: Board -> Int -> IO ()
putBoard cboard lnLength = putStr $ makeFramedPrint cboard lnLength

-- same but it assumes lineLength of 8, use this for testing
putDefaultBoard :: Board -> IO ()
putDefaultBoard cboard = putStr $ makeFramedPrint cboard 8

{-
Makes a pretty looking frame around the board when the board
is turned into a string. Takes in a board and an int for
lineLength.
-}
makeFramedPrint :: Board -> Int -> String
makeFramedPrint cboard lnLength =
  left ++ middle ++ corner ++ rest ++ "\n" ++ frameBottom
 where
  middle         = take ((lnLength * 2) - 1) $ cycle "═╤"
  left           = "   ╔"
  corner         = "╗\n"
  rest           = makePrint cboard lnLength
  bottomDividers = (take ((lnLength * 2) - 1) $ cycle "═╧")
  frameBottom    = "   ╚" ++ bottomDividers ++ "╝\n   " ++ columnIndices ++ " \n"
  columnIndices =
    concat $ take lnLength [ ' ' : (show x) | x <- [1 .. lnLength] ]

{-
Make a string for each row and merge them in a way which looks
nice for the body of the framed print. Takes in a board and a line
length integer.
-}
makePrint :: Board -> Int -> String
makePrint cboard lnLength =
  intercalate "\n" [makeRow cboard y lnLength |y <- [0..lnLength-1]]
--intercalate puts a character in between others in a list
--Sam - Why is this called makePrint?

{-
Make a string of the row specified by y in the board.
Takes in a line length (we need to know how long rows are).
-}
makeRow :: Board -> Int -> Int -> String
makeRow cboard y lnLength = columnNum ++ fancyRow ++ "║"
 where
  rawRow = [ case pieceOf cboard (x,y) of
                      Nothing -> " "
                      Just White -> whitePiece
                      Just Black -> blackPiece
           | x <- spaceRange]
  fancyRow = intercalate [column] rawRow
  column     = '│'
  whitePiece = "⛀"
  blackPiece = "⛂"
  spaceRange = [0 .. lnLength - 1]
  columnNum = if (y<=8) -- this logic may need review
              then (show $1+y)++"  ║"
              else (show $1+y)++" ║"

pieceOf :: Board -> Cord -> Maybe Piece
pieceOf cboard loc = ret
  where
    matchedSpaces = filter (\(cord,_) -> loc==cord) cboard
    ret = if 0 == length matchedSpaces
             then Nothing
          else case head matchedSpaces of
                 (_,White) -> Just White
                 (_,Black) -> Just Black
--probably should have used lookup instead
