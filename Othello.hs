module Othello where
import Data.Tuple                     ( swap )
import Data.List
import Data.Maybe
import OTypes

allDirections :: [Direction]
allDirections = [N, S, E, W, NE, NW, SE, SW]

--                           Milestone 1: Representation

-- check if a move is legal for current player and then do it
doMove :: Game -> Cord -> Maybe Game
doMove game cord =
  case newBoard of
    Nothing -> Nothing 
    Just bd ->
      if (newSpace `elem` possibleMoves)
      then Just Game {board=bd,lineLength=lineLn,currentPlayer=newPlayer,cords=bcords}
      else Nothing
  where
    oldPlayer = currentPlayer game
    bcords = cords game 
    lineLn = lineLength game
    possibleMoves = legalMoves game
    newSpace = (cord,oldPlayer)
    newPlayer = swapPiece oldPlayer
    newBoard = makeMove newSpace game

{-
The idea here with makeMove is to take a Space as
input (a tuple of cord and piece) and return the board which
results from playing that space on the board that was passed
into the function.
This function DOES NOT do any checks about whether
the move passed into it (the Space) is a valid move
according to the rules, that's what legalMoves does.
-}

makeMove :: Space -> Game -> Maybe Board
makeMove space game = 
  case checkMove game (fst space) of
        Just _ -> Just newBoard
        Nothing -> Nothing
 where
  oldBoard = board game
  chainMaker    = makeChain oldBoard space -- partial application
  newChains     = concat $ map chainMaker allDirections
  dedupedBoard  = filter (not . occupied newChains) oldBoard
  newBoard      = newChains ++ space:dedupedBoard

  
occupied :: [Space] -> Space -> Bool

occupied spaces (pos,_) = any (\(loc,_) -> pos == loc) spaces

{-
Start from the new space and make a list of Spaces (Chain)
by traversing out in the given direction on the given board.
The Chain argument expects an empty list.
-}

makeChain :: Board -> Space -> Direction -> Chain
makeChain cboard (cord,piece) direction = buildChain cboard direction nextSpace []
    where nextSpace = (nextCord, nextColor)
          nextCord  = travel direction cord
          nextColor = swapPiece piece

-- this builds a chain out in a direction while also swapping the
-- piece color of any spaces added to the chain.
buildChain :: Board -> Direction -> Space -> Chain -> Chain
buildChain cboard direction (cord,piece) acc =
 let nextSpace = ((travel direction cord), piece)
 in case readSpace cboard cord of
          Just thisPiece -> if(thisPiece /= piece)
                            then acc
                            else buildChain cboard direction nextSpace ((cord,swapPiece piece):acc)
          Nothing        -> []


readSpace :: Board -> Cord -> Maybe Piece
readSpace [] _ = Nothing
readSpace ((cord,piece):spaces) dest =
    if cord == dest
    then Just piece
    else readSpace spaces dest
{-
Modify the y coordinate according to the direction being traveled
anything which goes off the board will just not match a new space;
(in addSpace) so there are no special cases
-}

-- modify the X and Y coordinate according to the direction being traveled
travel :: Direction -> Cord -> Cord
travel dir (x,y) | dir == N = (x,y+1)
                 | dir == S = (x,y-1)
                 | dir == E = (x+1,y)
                 | dir == W = (x-1,y)
                 | dir == NE = (x+1,y+1)
                 | dir == NW = (x-1,y+1)
                 | dir == SE = (x+1,y-1)
                 | dir == SW = (x-1,y-1)
                 | otherwise = (x,y)


-- check if the "color" of two spaces is the same (ex: (_,Black) -> (_,Black) -> True)
--colorMatch :: Space -> Space -> Bool
--colorMatch (_, color1) (_, color2) = if color1 == color2 then True else False
--may be useful in future. DELETE if not used in final project

{-
findWinner figures when the game is ended. This will either be when there are no more Empty
spaces, or no legalMoves. If there are no more empty spaces, findWinner should find which Piece
(out of Black or White) has the larger number of Pieces on the Board. If there are no more
legalMoves, findWinner should also find which Piece has the larger number of Spaces on the Board,
Empty should be ignored as they are not a player.
-}
findWinner :: Board -> Winner
findWinner []    = error "No board, no winner"
findWinner cboard = case compare blkSpaces whteSpaces of
  GT -> Won Black
  LT -> Won White
  EQ -> Tie
 where
  blkSpaces  = length [ x | x <- cboard, snd x == Black ]
  whteSpaces = length [ x | x <- cboard, snd x == White ]

{-
legalMoves finds all the possiblle available spaces and uses helper functions
to determine the validity of each space according to various rules.

This gets a list of all empty coordinates on the board and calls them
existingLocs. Then legalMoves returns 

-}
legalMoves :: Game -> [Space]
legalMoves game = catMaybes possibleMoves
 where
   cboard = board game
   allCords = cords game
   possibleMoves = map (checkMove game) emptySpaces
   emptySpaces = [ crd | crd <- allCords, not (crd `elem` existingLocs) ]
   existingLocs = [ loc | (loc, _) <- cboard ]

{-
checkMove checks the the given player has a valid move they could make
at a given coordinate on the board. Returns a Just Space if they do,
otherwise returns a Nothing (no valid move at that coordinate for
the given player).
-}
checkMove :: Game -> Cord -> Maybe Space
checkMove game cord =
  let
    cboard = board game
    cplayer = currentPlayer game
    lnLength = lineLength game
    startPt = (cord, cplayer)
    radians = map (makeChain cboard startPt) allDirections
    changes = filter (not . null) radians --Use to have && flipCheck x
    validCord = if(((fst cord) `elem` [0..lnLength]) && ((snd cord `elem` [0..lnLength])))
                then True else False
    oldSpace = find (==startPt) cboard
    isOccupied = if (oldSpace==Nothing)
               then False else True
  in
    if (not isOccupied) && validCord && (length changes > 0)
    then Just (cord, cplayer) 
    else Nothing
    
swapPiece :: Piece -> Piece
swapPiece Black = White
swapPiece White = Black


        
{-
Milestone 2:
WhoWillWin and WhoHasWon
-}

-- return Nothing if the game is not over, otherwise findWinner
whoHasWon :: Game -> Maybe Winner
whoHasWon game =
  if (not $ gameOver game)
  then
    Nothing
  else -- the game must be over if the cPlayer has no moves
    Just $ findWinner $ board game

-- makes a list of all the possible future game states based on moves for currentPlayer
-- assuming since game is over legalMoves must return at least one non-Nothing
futureGames :: Game -> [Game]
futureGames game =
  let
    bcords = cords game 
    nextPlayer = swapPiece $ currentPlayer game
    lineLn = lineLength game
    possiblePlays = legalMoves game
    futureBoards = catMaybes $ map ((flip makeMove) game) possiblePlays
  in if (null possiblePlays)
     then [Game {board=(board game),lineLength=lineLn,currentPlayer=nextPlayer,cords=bcords}]
     else [Game {board=boardx,lineLength=lineLn,currentPlayer=nextPlayer,cords=bcords} | boardx <- futureBoards]

{-

whoWillWin takes a game and determines who will eventually win the game
in the future using recursion and looking for certain win states for
the currentPlayer.

(getAVictory explanation)
Using find allows for short-circuiting instead of checking whoWillWin
on the entire branch so we don't have to recurse over the whole
futureWinners if we find a single win state. A possible optimization
might be to sortBy the highest rating for currentPlayer before trying
find so that it may traverse fewer branches before short-circuiting.

Otherwise we just recurse over all the futureGames and check if it is
a tie or a loss (branchWinner).

-}

whoWillWin :: Game -> Winner
whoWillWin game =
  if (gameOver game) then findWinner $ board game -- game is already over, return winner
  else -- make a list of all possible future game states and check their winners
    case getAVictory of
      Just _ -> Won $ currentPlayer game
      Nothing ->
        let futureWinners = map whoWillWin fgames
        in branchWinner (currentPlayer game) futureWinners
  where
    fgames = rateSort (currentPlayer game) $ futureGames game
    getAVictory = find (\g -> (whoWillWin g) == (Won $ currentPlayer g)) fgames

-- the game from the other player's perspective
otherGame :: Game -> Game
otherGame game = Game (board game) (swapPiece $ currentPlayer game) (lineLength game) (cords game)

gameOver :: Game -> Bool
gameOver game = cPlayerDone && oPlayerDone
  where
    cPlayerDone = null $ legalMoves game
    oPlayerDone = null $ legalMoves $ otherGame game

gameAlmostOver :: Game -> Bool
gameAlmostOver game = cPlayerNearlyDone && (oPlayerDone || oPlayerNearlyDone)
  where
    cPlayerNearlyDone = null $ tail $ legalMoves game
    oGameMoves = legalMoves $ otherGame game
    oPlayerDone = null oGameMoves
    oPlayerNearlyDone = null $ tail oGameMoves
{- 
branchWinner takes a piece, a list of winners (as determined from the whoHasWon function), 
and returns the best possibility that that Piece has to win in this game.
-}

branchWinner :: Piece -> [Winner] -> Winner
branchWinner player lst | (Won player) `elem` lst = Won player 
                        | Tie `elem` lst          = Tie 
                        | otherwise               = Won (swapPiece player)

-- makes a list of all the possible future game states based on moves for currentPlayer
-- assuming since game is over legalMoves must return at least one non-Nothing
futureMoves :: Game -> [(Space,Game)]
futureMoves game =
  let
    bcords = cords game
    nextPlayer = swapPiece $ currentPlayer game
    lineLn = lineLength game
    possiblePlays = legalMoves game
    futureBoards = catMaybes $ map ((flip makeMove) game) possiblePlays
  in if (null possiblePlays) -- there were no valid moves but game isn't over yet
     then [(((-99,-99),currentPlayer game),Game {board=(board game),lineLength=lineLn,currentPlayer=nextPlayer,cords=bcords})]
     else
       (zip
        possiblePlays
        [Game {board=boardx,lineLength=lineLn,currentPlayer=nextPlayer,cords=bcords}
        | boardx <- futureBoards])
     
--- Very similair to whoWillWin
--- assumes game is already over and nextMoves exist
bestMove :: Game -> (Space, Winner)
bestMove game =
  -- there is only one move left, return the combo of that Space and if it wins
  if(gameAlmostOver game)
  then -- find the next game, which is the game that is over
    let
      -- gameAlmostOver confirms that the below will have only one result, no exceptions
      lastMove = head $ legalMoves game :: Space
      -- we know it is a legal Move, so there will never be a Nothing
      lastBoard = fromJust $ makeMove lastMove game
      finalWon = findWinner $ lastBoard
    in (lastMove,finalWon) -- lastMove was the move to make the win, so return that
  else
    let -- we still need to recurse to find the bestMove
      evaluatedGames = futureMoves game
      cPlayer = currentPlayer game
      moveResults = map (\(a,b) -> (a,whoWillWin b)) evaluatedGames
      winGames = filter (\(_,x) -> x==Won cPlayer) moveResults -- pairs with perfect wins
      tieGames = filter (\(_,x) -> x==Tie) moveResults -- pairs with ties
    in
      case -- watch the pretty syntax-snake go!
        ()
      of _
           | not $ null winGames -> head winGames
           | not $ null tieGames -> head tieGames
           | otherwise -> head moveResults -- this will have something if the others have none

goodMove :: Game -> Int -> (Maybe Space, Rating)
goodMove game limit = 
  let
    bcords = cords game
    nextPlayer = swapPiece (currentPlayer game)
    possiblePlays = legalMoves game
    makeMv = flip makeMove 
    futureBoards = [(pp, (fromJust (makeMv game pp))) | pp <- possiblePlays]
    lineLn = lineLength game
    newGame b = Game {board=b,lineLength=lineLn,currentPlayer=nextPlayer,cords=bcords} 
    moveComprehension = [(Just sp, scoreBranch (newGame b) limit) | (sp,b) <- futureBoards]
  in if (gameOver game)
     then (Nothing, Score 0)
     else if (gameAlmostOver game) 
          then case (currentPlayer game) of
                 White -> head moveComprehension 
                 Black -> head moveComprehension
     else case (currentPlayer game) of
            White -> maximumBy (\ (_, Score x) (_, Score y) -> compare x y) moveComprehension
            Black -> minimumBy (\ (_, Score x) (_, Score y) -> compare x y) moveComprehension

scoreBranch :: Game -> Int -> Rating
scoreBranch game 0 =
  case (currentPlayer game) of
    White -> snd $ maximumBy (\ (_, Score x) (_, Score y) -> compare x y)(map rateGame (futureGames game)) 
    Black -> snd $ minimumBy (\ (_, Score x) (_, Score y) -> compare x y)(map rateGame (futureGames game))
scoreBranch game limit =
  if (gameOver game)
  then snd $ rateGame game
  else case (currentPlayer game) of  -- for all future games - score all the games and then find the max score
         White -> scorePlus (snd (rateGame game)) (maximum [(scoreBranch g (limit-1)) | g <- futureGames game])
         Black -> scorePlus (snd (rateGame game)) (minimum [(scoreBranch g (limit-1)) | g <- futureGames game])

{-
---------------- Ratings and whoMightWin ---------------
-}

-- add the scores of two different ratings
scorePlus :: Rating -> Rating -> Rating
scorePlus (Score a) (Score b) = Score (a+b)


{-
Where Int represents an integer s.t. its magnitude is
the likelihood of a win.
If the number is positive the odds favor the White piece, and
if the number is negative the odds favor the Black piece.
-}

-- rate an individual space, negative is good for black and positive is good for white
rateSpace :: Game -> Space -> Int
rateSpace game (cord,p) =
  case (isEdge,p) of
    (True , Black) -> -4
    (True , White) -> 4
    (False , Black) -> -1
    (False , White) -> 1
  where
    -- get the cords around the current space
    surroundingCords = map ((flip travel) cord) allDirections
    -- check if any surrounding spaces are empty
    isEdge = any (\loc -> (readSpace (board game) loc) == Nothing) surroundingCords
    
-- Give a game a rating and return a tuple with the game and its rating

rateGame :: Game -> ScoredGame
rateGame game = (game,Score gameScore)
  where gameScore =
          if (gameOver game)
          then
            let winner = findWinner $ board game
            in
              case winner of
                Won White -> 257
                Won Black -> -257
                Tie -> 0 -- this shouldn't happen, but to make the compiler happy
          else sum $ map (rateSpace game) $ board game


{-
Sort a list of games by how they favor the piece/player passed in.
This is used as a heuristic for whoWillWin and whoMightWin.
By sorting the list that find runs through with recursing the odds of
terminating with an winning state early probably increases.

Probably because who knows if this rating function is good anyways.
-}
rateSort :: Piece -> [Game] -> [Game]
rateSort p games = map fst sortedGames
  where scores = map rateGame games
        sortedGames = sortBy (\(_,(Score a)) (_,(Score b)) -> rateCompare p a b) scores

-- custom compare which depends on the piece passed in
rateCompare :: Ord a => Piece -> a -> a -> Ordering
rateCompare p a b =
  case p of
    White -> compare a b
    Black -> compare b a
    
{-
tl;dr Int is the bounding depth
Takes a game and operates recursively solving the tree of possible games,
however this time if the integer argument becomes zero or lower instead
of continuing to recurse a guess of which player might win the branch is made.
whoMightWin :: Game -> Int -> Rating
-}
whoMightWin :: Game -> Int -> Winner
whoMightWin game depth =
  if (gameOver game) then findWinner $ board game -- game is already over, return winner
  else if (depth>1) then  -- make a list of all possible future game states
    case getAVictory of
      Just g -> Won $ currentPlayer game
      Nothing ->
        let futureWinners = map ((flip whoMightWin) (depth-1)) fgames
        in branchWinner (currentPlayer game) futureWinners
  else -- "We're in too deep" , lets just do a dirty prediction based on this set of futureGames
    let
      futureRatings = map (snd . rateGame) fgames
    in branchRating futureRatings
  where
    fgames = rateSort (currentPlayer game) $ futureGames game
    getAVictory = find (\g -> (whoMightWin g (depth-1)) == (Won $ currentPlayer g)) fgames
        
{-
Given a list of Ratings determine the likely winner when averaging all cases
-}
branchRating :: [Rating] -> Winner
branchRating lst =
  let
    intlst = map (\ (Score i) -> i) lst
    branchAvg = (sum intlst) `div` (length lst)
  in case () of
      _ | branchAvg == 0 -> Tie
        | branchAvg > 0 -> Won White
        | otherwise -> Won Black
