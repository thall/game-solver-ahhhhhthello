module OTypes where

--                                   Data Types

type Cord = (Int, Int)


data Piece = White | Black deriving (Show, Eq)

type Space = (Cord, Piece)

type Board = [Space]

-- Chain is not a complete board, but just a collection of spaces
type Chain = [Space]

data Direction = N | S | W | E | NE | NW | SE | SW deriving (Show, Eq)

type Player = Piece

data Winner = Won Piece | Tie deriving (Show, Eq)

-- Game type:
data Game = Game { board::Board
                 , currentPlayer::Player
                 , lineLength::Int
                 , cords::[Cord]
                 }

-- types for rating games in lazy guesses

data Rating = Score Int deriving (Eq, Ord, Show)
type ScoredGame = (Game, Rating)
