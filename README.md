# othelloquo
## Othello Game Solver
 This project is:
 - An interactive game of Othello with an AI player which can be played on boards of arbitrary size.
 - An Othello solver/predictor which can give you the best move for a game state on boards of arbitrary size.
-------------------------------------------
## Initial Contributors
Here is a list of my group that originally put together the project with their github usernames
- Me - thall-gnu
- Monica Lampton - mlampton
- Nash Evans - Revans1
- Sam Wallace - swallac4

-------------------------------------------
## Compiling and running:
- compile with `ghc Main.hs`
- run the `Main` which gets output with `./Main` and any relevant flags (`-h` for help)
  - to play a regular Othello game run `./Main -i boards/startboard`
  - to play a large/small game run `./Main -i boards/tinystart` and `./Main -i boards/largestart` respectively

-------------------------------------------
## todo:
- [ ] refactoring
  - [ ] Types all in Othello.hs and Othello.hs organized
  - [ ] Make a Solver.hs for the solving/AI functions
  - [ ] Limit public scope of functions in Solver.hs and Othello.hs
  - [ ] Optimize `goodMove` by letting it take advatage of `whoMightWin`'s optimizations
- [ ] makefile
